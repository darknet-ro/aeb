<?php

/*
 * Alexa Voice service/EasyBulb PHP Integration for home automation
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Aranyi Csaba  <acsaba@darknet.ro>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
*/
require 'power.php';
require 'Milight.php';
//the ip address of the easy bulb controller.
$milight = new Milight('192.168.2.50');

$EchoJArray = json_decode(file_get_contents('php://input'));
$RequestType = $EchoJArray->request->type;

$JsonOut = GetJson($RequestType, $EchoJArray);
$size = strlen($JsonOut);
header('Content-Type: application/json');
header("Content-length: $size");
echo $JsonOut;

function GetJson($RequestMessageType, $EchoJArray) {
    global $milight;
    $RequestId = $EchoJArray->request->requestId;
    $ReturnValue = "";

    if ($RequestMessageType == "LaunchRequest") {
        $reprompt = 'You can turn on and off the light, change color ot the light, ask for power consumption and many other nice features.';
        $SpeakPhrase = 'Welcome to my home automation assistent.';
        $ret = array("currentValue" => 0);
        $ReturnValue = interact($ret, $SpeakPhrase, $reprompt);
    }

    if ($RequestMessageType == "SessionEndedRequest") {
        $ReturnValue = '{
		  "type": "SessionEndedRequest",
		  "requestId": "$RequestId",
		  "timestamp": "' . date("c") . '",
		  "reason": "USER_INITIATED "
		}
		';
    }

    if ($RequestMessageType == "IntentRequest") {

        $NextNumber = 0;
        $EndSession = "false";
        $SpeakPhrase = "";
        $reprompt = 'You can turn on and off the light, change color of the light, ask for power consumption and many other nice features.';
        if ($EchoJArray->request->intent->name == "switch") {
            if ($EchoJArray->request->intent->slots->action->value == "on") {
                if ($EchoJArray->request->intent->slots->group->value == "2") {
                    turnnon(2);
                    $SpeakPhrase = "The light is On on group 2";
                } else {
                    if ($EchoJArray->request->intent->slots->group->value == "3") {
                        turnnon(3);
                        $SpeakPhrase = "The light is On on group 3";
                    } else {
                        turnnon();
                        $SpeakPhrase = "The light is On";
                    }
                }
            }

            if ($EchoJArray->request->intent->slots->action->value == "off") {
                if ($EchoJArray->request->intent->slots->group->value == "2") {
                    turnnoff(2);
                    $SpeakPhrase = "The light is off on group 2";
                } else {
                    if ($EchoJArray->request->intent->slots->group->value == "3") {
                        turnnoff(3);
                        $SpeakPhrase = "The light is off on group 3";
                    } else {
                        turnnoff();
                        $SpeakPhrase = "The light is off";
                    }
                }

               // $reprompt = 'you can say on or off to change the light';
                $NextNumber = $EchoJArray->session->attributes->sessionVariables->currentValue + 1;
                $ret = array("currentValue" => $NextNumber);
                $ReturnValue = interact($ret, $SpeakPhrase, $reprompt);
            }
        }


        if ($EchoJArray->request->intent->name == "changecolor") {

            if ($EchoJArray->request->intent->slots->color->value == "white") {
                $milight->rgbwAllSetToWhite();
                $SpeakPhrase = "The light is white ";
            }
            
            if ($EchoJArray->request->intent->slots->color->value == "blue") {
                $milight->rgbwSetColorHexString('0000FF');
                $SpeakPhrase = "The light is blue ";
            }

            if ($EchoJArray->request->intent->slots->color->value == "red") {
                $milight->rgbwSetColorHexString('FF0000');
                $SpeakPhrase = "The light is red ";
            }
        }
        
            if ($EchoJArray->request->intent->name == "report") {

            if ($EchoJArray->request->intent->slots->powertype->value == "instant") {
                $gi=getInstant();
                if ($gi > 0 )
                {
                     $SpeakPhrase = "The instant power consumption is  " . $gi*230 . " watt";
                }
                $reprompt = NULL;
                
            }
            
            if ($EchoJArray->request->intent->slots->powertype->value == "summarized") {
                 $ga=getAvr();
                 $SpeakPhrase = "The average power consumption is  " . $ga*230 . " watt";
            }
                $reprompt = NULL;

        }
        
        // $reprompt = 'you can say on or off to change turn on / off light or you can ask report summarized power consumption or report instant power consumption';
     
        $NextNumber = $EchoJArray->session->attributes->sessionVariables->currentValue + 1;
        $ret = array("currentValue" => $NextNumber);
        $ReturnValue = interact($ret, $SpeakPhrase, $reprompt);
        $SpeakPhrase = "The light is On on group 2";
    }

    return $ReturnValue;
}

function turnnon($group) {
    global $milight;
    if (isset($group) && $group != '') {
        if ($group == 2) {
            $milight->rgbwGroup2On();
        }
        if ($group == 3) {
            $milight->rgbwGroup3On();
        }
    } else {
        $milight->rgbwAllOn();
    }
}

function turnnoff($group) {
    global $milight;
    if (isset($group) && $group != '') {
        if ($group == 2) {
            $milight->rgbwGroup2Off();
        }
        if ($group == 3) {
            $milight->rgbwGroup3Off();
        }
    } else {
        $milight->rgbwAllOff();
    }
}

// end function GetJsonMessageResponse

function interact($sessionVariables, $response, $reprompt) {

    $ReturnValue = '';
    $mysessionVariables = '';
    $i = 0;
    $numItems = count($sessionVariables);
    foreach ($sessionVariables as $Key => $Value) {
        $mysessionVariables = $mysessionVariables . '"' . $Key . '" : ' . $Value;
        if (++$i !== $numItems) {
            $mysessionVariables = $mysessionVariables . ',';
        }
    }

    $ReturnValue = '
		{
		  "version": "1.0",
		  "sessionAttributes": {
			"sessionVariables": {
			  ' . $mysessionVariables . '
			}
		  },
		  "response": {
			"outputSpeech": {
			  "type": "PlainText",
			  "text": "' . $response . '"
			},
			"card": {
			  "type": "Simple",
			  "title": "' . $response . '",
			  "content": "' . $response . '"
			},';

    if (isset($reprompt) && ($reprompt != null || $reprompt != '')) {
        $ReturnValue = $ReturnValue . '"reprompt": {
			  "outputSpeech": {
				"type": "PlainText",
				"text": "' . $reprompt . '"
			  }
			}, "shouldEndSession": false';
    } else {
        $ReturnValue = $ReturnValue . '"shouldEndSession": true';
    }


    $ReturnValue = $ReturnValue . '}}';

    return $ReturnValue;
}
