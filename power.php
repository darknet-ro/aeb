<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$url = 'http://192.168.2.177/';

// var_dump($array);



function getInstant() {
    global $url;

    $ch = curl_init();
    $timeout = 5;

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

// Get URL content
    $lines_string = curl_exec($ch);
// close handle to release resources
    curl_close($ch);
//output, you can also save it locally on the server
// echo $lines_string;
    $array = explode(PHP_EOL, $lines_string);
    //print_r($array);
    $r = 0;
    foreach ($array as  &$line) { // loop line by line and convert into array
     //   echo $line;
        if (strpos($line, 'IRMS') !== false) {
            $i = explode(" ", $line);
            $r = $i[1];
        }
        
    }
    return $r;
}

function getAvr() {
    global $url;

    $ch = curl_init();
    $timeout = 5;

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

// Get URL content
    $lines_string = curl_exec($ch);
// close handle to release resources
    curl_close($ch);
//output, you can also save it locally on the server
// echo $lines_string;
    $array = explode(PHP_EOL, $lines_string);
    //print_r($array);
    $r = 0;
    foreach ($array as  &$line) { // loop line by line and convert into array
     //   echo $line;
        if (strpos($line, 'Media since') !== false) {
            $i = explode("-", $line);
            $r = $i[1];
        }
        
    }
    return $r;
   
}
